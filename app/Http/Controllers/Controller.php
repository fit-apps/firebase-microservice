<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\FirebaseStoreJob;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Laravel\Lumen\Routing\Controller as BaseController;
use Usoft\RabbitMq\Services\SendService;

class Controller extends BaseController
{

    public function store(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'token' => 'required',
        ]);

        try {
            (new SendService())->queue_declare('firebase', [
                'user_id' => $user->id,
                'token' => $request->input('token'),
                'device_id' => $request->input('device_id'),
                'user_agent' => $request->userAgent()
            ]);
//            $this->dispatchNow(new FirebaseStoreJob($request, $user));//->onQueue('firebase_tokens');
        } catch (\Exception $exception) {
            Log::error("Firebase dispatch in job error: {$exception->getMessage()}");
            return response()->json([
                'message' => "try again"
            ], 403);
        }

        return response('', 201);
    }
}
