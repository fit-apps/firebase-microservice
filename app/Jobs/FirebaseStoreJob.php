<?php

namespace App\Jobs;

use App\Models\Firebase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FirebaseStoreJob extends Job
{

    protected $user;
    protected $token;

    public function __construct($request, $user)
    {
        $this->user = $user;
        $this->token = $request->input('token');
    }

    public function handle()
    {
        DB::beginTransaction();
        try {
            Firebase::query()->create([
                'user_id' => $this->user->id,
                'token' => $this->token
            ]);
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("STORE JOB {$exception->getMessage()}");
            abort(403);
        }

        DB::commit();
    }
}
