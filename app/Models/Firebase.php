<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Firebase extends Model
{

    protected $connection = 'firebase';
    protected $table = 'firebase';

    protected $guarded = ['id'];

    protected $casts = [
        'token' => 'string',
        'user_id' => 'integer'
    ];

}
