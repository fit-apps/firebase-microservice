<?php

return [
    'default' => 'firebase',
    'migrations' => 'migrations',

    'connections' => [
        'firebase' => [
            'driver' => env('DB_CONNECTION', 'mysql'),
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', 3306),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),

            'charset' => 'utf8',
            'prefix' => '',
            'schema' => env('DB_SCHEMA', 'public'),
            'prefix_indexes' => true,
            'search_path' => 'public',
            'sslmode' => 'prefer'
        ],

        'users' => [
            'driver' => 'pgsql',
            'host' => env('DB_USERS_HOST', '127.0.0.1'),
            'port' => env('DB_USERS_PORT', 3306),
            'database' => env('DB_USERS_DATABASE', 'forge'),
            'username' => env('DB_USERS_USERNAME', 'forge'),
            'password' => env('DB_USERS_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => env('DB_SCHEMA', 'public'),
            'prefix_indexes' => true,
            'search_path' => 'public',
            'sslmode' => 'prefer'
        ],

    ],


    'redis' => [

        'cluster' => false,

        'default' => [
            'host' => env('REDIS_HOST', '192.168.0.3'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

        'cache' => [
            'host' => env('REDIS_HOST', '192.168.0.3'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

        'options' => [
            'parameters' => ['password' => env('REDIS_CACHE_PASSWORD', null)],
        ],

    ],
];
